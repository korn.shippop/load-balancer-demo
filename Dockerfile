FROM node:14.15.4-alpine

WORKDIR /Users/kornchotpitakkul/Desktop/load-balancer-demo

COPY package*.json /Users/kornchotpitakkul/Desktop/load-balancer-demo

RUN npm install

COPY . /Users/kornchotpitakkul/Desktop/load-balancer-demo