'use strict'

const os = require('os');

const express = require('express');
const app = express();

let count = 0;

app.get('/:greet', (req, res) => {
    console.log(`${req?.params?.greet} ${count++}`);
    
    return res.sendStatus(200);
});

app.listen(5000, () => console.log(`CONTAINER - ${os.hostname()}`));

process
    .on('unhandledRejection', (reason, p) => {
        console.error(reason, 'Unhandled Rejection at Promise', p);
    })
    .on('uncaughtException', err => {
        console.error(err, 'Uncaught Exception thrown');

        process.exit(1);
    });